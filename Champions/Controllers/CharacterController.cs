﻿using Champions.DAL.Implementations;
using Champions.Models.Characters;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Champions.Controllers
{
    public class CharacterController : Controller
    {
        [HttpGet]
        public ActionResult CharacterList()
        {
            CharacterContext context = new CharacterContext();
            CharacterRepository repository = new CharacterRepository(context);

            List<DataCharacter> characterlist = repository.GetAllPreview();

            return View(characterlist);
        }

        [HttpGet]
        public ActionResult Details(int id)
        {
            CharacterContext context = new CharacterContext();
            CharacterRepository repository = new CharacterRepository(context);

            DataCharacter character = repository.GetCharacter(id);
            return View(character);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            CharacterRepository characterRepository = new CharacterRepository(new CharacterContext());
            DataCharacter character = characterRepository.GetCharacter(id);

            WeaponRepository weaponRepository = new WeaponRepository(new WeaponContext());

            string weaponType = character.EquippedWeapon.WeaponType.ToString();
            List<string> weapons = weaponRepository.GetWeaponNamesOfType(weaponType);

            List<SelectListItem> weaponItems = new List<SelectListItem>();
            foreach(string s in weapons)
            {
                SelectListItem item = new SelectListItem
                {
                    Text = s
                };
                weaponItems.Add(item);
            }
            ViewData["Weapons"] = weaponItems;

            ArmorRepository armorRepository = new ArmorRepository(new ArmorContext());
            List<string> armors = armorRepository.GetAllNames();

            List<SelectListItem> armorItems = new List<SelectListItem>();
            foreach (string s in armors)
            {
                SelectListItem item = new SelectListItem
                {
                    Text = s
                };
                armorItems.Add(item);
            }
            ViewData["Armors"] = armorItems;

            return View(character);
        }

        [HttpPost]
        public ActionResult Edit(int id, string name, string characterClass, int level)
        {
            DataCharacter character = new DataCharacter(
                id,
                name,
                1,
                "Warrior",
                new int[]
                {
                    0,0,0,0,0
                },
                null,
                null,
                null,
                20,
                false
            );
            // Validate data and create character.

            // If data isn't valid, go back to Edit view

            // Update character in Database
            CharacterContext context = new CharacterContext();
            CharacterRepository repository = new CharacterRepository(context);

            repository.UpdateCharacter(character);

            // Return view of updated character.

            return View("Details", character);
        }
    }
}

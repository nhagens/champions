﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Champions.Interfaces.Items
{
    public interface IWeapon
    {

        int DamageDieSides { get; }
        int DamageDieAmount { get; }
    }
}

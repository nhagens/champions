﻿using Champions.Models;
using System.Collections.Generic;

namespace Champions.Interfaces
{
    public interface IBaseCharacter
    {
        string Name { get; }
        int Experience { get; }
        int Level { get; }
        string CharacterClass { get; }
        int[] Stats { get; }
        IWeapon EquippedWeapon { get; }
        IArmor EquippedArmor { get; }
        List<IPotion> Potions { get; }
    }
}

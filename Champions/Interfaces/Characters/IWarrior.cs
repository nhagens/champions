﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Champions.Interfaces.Characters
{
    interface IWarrior
    {
        void WarriorAttack(ICharacter target);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Champions.Interfaces.Characters
{
    interface IBattleCharacter
    {
        int Hitpoints { get; }
        int Actionpoints { get; }
        int ArmorClass { get; }
        bool CurrentTurn { get; }
        Point Position { get; }
        
        void TakeDamage(int damage);
        void Move(Point point);
    }
}

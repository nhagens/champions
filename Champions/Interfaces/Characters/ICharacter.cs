﻿using Champions.Interfaces.Items;
using Champions.Models.Items;
using System.Collections.Generic;

namespace Champions.Interfaces.Characters
{
    public interface ICharacter
    {
        string Name { get; }
        int Level { get; }
        string CharacterClass { get; }
        int[] Stats { get; }
        Weapon EquippedWeapon { get; }
        Armor EquippedArmor { get; }
        List<Potion> Potions { get; }
    }
}

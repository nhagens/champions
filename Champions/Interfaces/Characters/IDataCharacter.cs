﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Champions.Interfaces.Characters
{
    interface IDataCharacter
    {
        int ID { get; }
        int Gold { get; }
        bool IsDefaultCharacter { get; }
    }
}

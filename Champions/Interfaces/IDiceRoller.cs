﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Champions.Interfaces
{
    public interface IDiceRoller
    {
        int RandomSeed { get; }
        int DiceAmount { get; }
        int DiceSides { get; }

        int RollDice();
    }
}

﻿using Champions.Interfaces.Characters;
using System.Collections.Generic;

namespace Champions.Interfaces
{
    public interface ITurnOrder
    {
        int RoundCount { get; }
        int TurnIndex { get; }
        List<ICharacter> Characters { get; }

        void NextTurn();
        void NewRound();
    }
}

﻿using Champions.Interfaces.Items;
using System;

namespace Champions.Models.Items
{
    public enum EnumWeaponType
    {
        Staff,
        Sword,
        Bow
    }

    public class Weapon : IItem, IWeapon
    {
        public string Name { get; private set; }

        public int Value { get; private set; }

        public EnumWeaponType WeaponType {get; private set; }

        public int DamageDieSides { get; private set; }

        public int DamageDieAmount { get; private set; }

        public Weapon(string name, int value, string weaponType, int damageDieSides, int damageDieAmount)
        {
            Name = name;
            Value = value;
            WeaponType = (EnumWeaponType)Enum.Parse(typeof(EnumWeaponType), weaponType);
            DamageDieSides = damageDieSides;
            DamageDieAmount = damageDieAmount;
        }

        public void Sell()
        {
            throw new System.NotImplementedException();
        }
    }
}
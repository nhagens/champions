﻿using Champions.Interfaces.Items;

namespace Champions.Models.Items
{
    public class Armor : IItem, IArmor
    {
        private string name = "Armor";
        private int armorBonus = 2;
        private int value = 10;

        public string Name { get { return name; } }
        public int Value { get { return value; } }
        public int ArmorBonus { get { return armorBonus; } }

        public Armor(string name, int value, int armorBonus)
        {
            this.name = name;
            this.value = value;
            this.armorBonus = armorBonus;
        }
    }
}
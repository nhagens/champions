﻿using Champions.Interfaces.Items;

namespace Champions.Models.Items
{
    public class Potion : IItem, IPotion
    {
        private string name = "potion";
        private int value = 10;
        private int healingDieSides = 4;
        private int healingDieAmount = 1;

        public string Name { get { return name; } }

        public int Value { get { return value; } }

        public int HealingDieSides { get { return healingDieSides; } }

        public int HealingDieAmount { get { return healingDieAmount; } }

        public Potion(string name, int value, int healingDieSides, int healingDieAmount)
        {
            this.name = name;
            this.value = value;
            this.healingDieSides = healingDieSides;
            this.healingDieAmount = healingDieAmount;
        }
    }
}
﻿using Champions.Interfaces;
using System;

namespace Champions.Models
{
    public class DiceRoller : IDiceRoller
    {
        private int diceAmount;
        private int diceSides;

        public int RandomSeed { get; set; }
        public int DiceAmount { get { return diceAmount; } }
        public int DiceSides { get { return diceSides; } }

        public DiceRoller(int diceAmount, int diceSides)
        {
            RandomSeed = new Random(Guid.NewGuid().GetHashCode()).Next();

            this.diceAmount = diceAmount;
            this.diceSides = diceSides;
        }

        public int RollDice()
        {
            Random random = new Random(Guid.NewGuid().GetHashCode());

            int result = random.Next(diceAmount, diceSides * diceAmount);

            return result;
        }
    }
}
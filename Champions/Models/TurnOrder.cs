﻿using Champions.Interfaces;
using Champions.Interfaces.Characters;

namespace Champions.Models
{
    public class TurnOrder : ITurnOrder
    {
        public int RoundCount => throw new System.NotImplementedException();

        public int TurnIndex => throw new System.NotImplementedException();

        public System.Collections.Generic.List<ICharacter> Characters => throw new System.NotImplementedException();

        public void NewRound()
        {
            throw new System.NotImplementedException();
        }

        public void NextTurn()
        {
            throw new System.NotImplementedException();
        }
    }
}
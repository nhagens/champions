﻿using Champions.Interfaces;
using Champions.Interfaces.Characters;
using Champions.Interfaces.Items;
using Champions.Models.Items;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;

namespace Champions.Models.Characters
{
    public class Mage : ICharacter, IMage
    {
        public string Name => throw new NotImplementedException();

        public int Level => throw new NotImplementedException();

        public string CharacterClass => throw new NotImplementedException();

        public int[] Stats => throw new NotImplementedException();

        public Weapon EquippedWeapon => throw new NotImplementedException();

        public Armor EquippedArmor => throw new NotImplementedException();

        public List<Potion> Potions => throw new NotImplementedException();

        public void MageAttack(Point point)
        {
            throw new NotImplementedException();
        }
    }
}
﻿using System.Collections.Generic;
using Champions.Interfaces.Characters;
using Champions.Interfaces.Items;
using Champions.Models.Items;

namespace Champions.Models.Characters
{
    public class DataCharacter : ICharacter, IDataCharacter
    {
        private int id;
        private string name;
        private int level;
        private string characterClass;
        private int[] stats;
        private Weapon equippedWeapon;
        private Armor equippedArmor;
        private List<Potion> potions;
        private int gold;
        private bool isDefaultCharacter;

        public int ID { get { return id; } }
        public string Name { get { return name; } }
        public int Level { get { return level; } }
        public string CharacterClass { get { return characterClass; } }
        public int[] Stats { get { return stats; } }
        public Weapon EquippedWeapon { get { return equippedWeapon; } }
        public Armor EquippedArmor { get { return equippedArmor; } }
        public List<Potion> Potions { get { return potions; } }
        public int Gold { get { return gold; } }
        public bool IsDefaultCharacter { get { return isDefaultCharacter; } }

        public DataCharacter(int id, string name, int level, string characterClass, int[] stats, Weapon equippedWeapon, Armor equippedArmor, List<Potion> potions, int gold, bool isDefaultCharacter)
        {
            this.id = id;
            this.name = name;
            this.level = level;
            this.characterClass = characterClass;
            this.stats = stats;
            this.equippedWeapon = equippedWeapon;
            this.equippedArmor = equippedArmor;
            this.potions = potions;
            this.gold = gold;
            this.isDefaultCharacter = isDefaultCharacter;
        }
    }
}
﻿using Champions.Interfaces;
using Champions.Interfaces.Characters;
using Champions.Interfaces.Items;
using Champions.Models.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Champions.Models.Characters
{
    public class Warrior : ICharacter, IWarrior
    {
        public string Name => throw new NotImplementedException();

        public int Level => throw new NotImplementedException();

        public string CharacterClass => throw new NotImplementedException();

        public int[] Stats => throw new NotImplementedException();

        public Weapon EquippedWeapon => throw new NotImplementedException();

        public Armor EquippedArmor => throw new NotImplementedException();

        public List<Potion> Potions => throw new NotImplementedException();

        public void WarriorAttack(ICharacter target)
        {
            throw new NotImplementedException();
        }
    }
}
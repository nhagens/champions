﻿using Champions.DAL.Interfaces;
using Champions.Interfaces.Items;
using Champions.Models.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Champions.DAL.Implementations
{
    public class WeaponRepository : IWeaponRepository
    {
        private readonly IWeaponContext _context;

        public WeaponRepository(IWeaponContext context)
        {
            _context = context;
        }

        public List<string> GetAllNames()
        {
            return _context.GetAllNames();
        }

        public List<string> GetWeaponNamesOfType(string weaponType)
        {
            return _context.GetWeaponNamesOfType(weaponType);
        }

        public Weapon GetWeapon(int id)
        {
            throw new NotImplementedException();
        }
    }
}
﻿using Champions.DAL.Interfaces;
using Champions.Models.Characters;
using Champions.Models.Items;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Champions.DAL.Implementations
{
    public class CharacterContext : ICharacterContext
    {
        private readonly string connectionstring = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;

        public void Add(DataCharacter character)
        {
            throw new NotImplementedException();
            
        }

        public void Remove(DataCharacter character)
        {
            throw new NotImplementedException();
        }

        public void Update(DataCharacter character)
        {
            using (SqlConnection con = new SqlConnection(connectionstring))
            {
                con.Open();

                SqlCommand cmd = new SqlCommand("UpdateCharacter", con)
                {
                    CommandType = CommandType.StoredProcedure
                };

                cmd.Parameters.Add("@id", SqlDbType.Int);
                cmd.Parameters.Add("@name", SqlDbType.VarChar);
                cmd.Parameters.Add("@equippedWeapon", SqlDbType.Int);
                cmd.Parameters.Add("@equippedArmor", SqlDbType.Int);

                cmd.Parameters["@id"].Value = character.ID;
                cmd.Parameters["@name"].Value = character.Name;
                cmd.Parameters["@equippedWeapon"].Value = 1;
                cmd.Parameters["@equippedArmor"].Value = 1;

                int affectedRows = cmd.ExecuteNonQuery();

                Console.WriteLine("Afftected lines: " + affectedRows);

                con.Close();
            }
        }

        public List<DataCharacter> GetAllPreview()
        {
            List<DataCharacter> characterlist = new List<DataCharacter>();

            using (SqlConnection con = new SqlConnection(connectionstring))
            {
                con.Open();

                SqlCommand cmd = new SqlCommand("GetAllCharactersPreview", con)
                {
                    CommandType = CommandType.StoredProcedure
                };

                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter
                {
                    SelectCommand = cmd
                };
                da.Fill(dt);

                foreach (DataRow row in dt.Rows)
                {
                    DataCharacter character = new DataCharacter(
                        (int) row["CharacterID"],
                        (string) row["Name"],
                        (int) row["Level"],
                        (string) row["Class"],
                        null,
                        null,
                        null,
                        null,
                        0,
                        (bool) row["IsDefaultCharacter"]
                    );

                    characterlist.Add(character);
                }

                con.Close();
            }

            return characterlist;
        }

        public DataCharacter GetCharacter(int id)
        {
            DataCharacter character = null;

            using (SqlConnection con = new SqlConnection(connectionstring))
            {
                con.Open();

                SqlCommand cmd = new SqlCommand("GetCharacter", con);
                cmd.Parameters.Add("@id", SqlDbType.Int);
                cmd.Parameters["@id"].Value = id;

                cmd.CommandType = CommandType.StoredProcedure;

                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter
                {
                    SelectCommand = cmd
                };
                da.Fill(dt);

                foreach (DataRow row in dt.Rows)
                {
                    character = new DataCharacter(
                        (int)row["CharacterID"],
                        (string)row["Name"],
                        (int)row["Level"],
                        (string)row["Class"],
                        new int[] {
                            (int) row["Strength"],
                            (int) row["Defense"],
                            (int) row["Constitution"],
                            (int) row["Initiative"],
                            (int) row["Speed"]
                        },
                        new Weapon((string)row["WeaponName"], 0, (string)row["WeaponType"],0,0),
                        new Armor((string)row["ArmorName"], 0, 0),
                        null,
                        (int)row["Gold"],
                        (bool)row["IsDefaultCharacter"]
                    );
                }

                con.Close();
            }

            return character;
        }
    }
}
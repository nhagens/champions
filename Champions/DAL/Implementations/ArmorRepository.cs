﻿using Champions.DAL.Interfaces;
using Champions.Models.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Champions.DAL.Implementations
{
    public class ArmorRepository : IArmorRepository
    {
        private readonly IArmorContext _context;

        public ArmorRepository(IArmorContext context)
        {
            _context = context;
        }

        public Armor GetArmor(int id)
        {
            throw new NotImplementedException();
        }

        public List<string> GetAllNames()
        {
            return _context.GetAllNames();
        }
    }
}
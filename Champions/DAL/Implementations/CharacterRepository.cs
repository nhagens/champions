﻿using Champions.DAL.Interfaces;
using Champions.Models.Characters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Champions.DAL.Implementations
{
    public class CharacterRepository : ICharacterRepository
    {
        private readonly ICharacterContext _context;

        public CharacterRepository(ICharacterContext context)
        {
            _context = context;
        }

        public void Add(DataCharacter character)
        {
            _context.Add(character);
        }

        public void Remove(DataCharacter character)
        {
            _context.Remove(character);
        }

        public void Update(DataCharacter character)
        {
            _context.Update(character);
        }

        public List<DataCharacter> GetAllPreview()
        {
            return _context.GetAllPreview();
        }

        public DataCharacter GetCharacter(int id)
        {
            return _context.GetCharacter(id);
        }

        public void UpdateCharacter(DataCharacter character)
        {
            _context.Update(character);
        }
    }
}
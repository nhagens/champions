﻿using Champions.DAL.Interfaces;
using Champions.Models.Items;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Champions.DAL.Implementations
{
    public class ArmorContext : IArmorContext
    {
        private readonly string connectionstring = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;

        public List<string> GetAllNames()
        {
            List<string> armorNames = new List<string>();

            using (SqlConnection con = new SqlConnection(connectionstring))
            {
                con.Open();

                SqlCommand cmd = new SqlCommand("GetAllArmorNames", con)
                {
                    CommandType = CommandType.StoredProcedure
                };

                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter
                {
                    SelectCommand = cmd
                };
                da.Fill(dt);

                foreach (DataRow row in dt.Rows)
                {
                    armorNames.Add((string)row["Name"]);
                }

                con.Close();
            }

            return armorNames;
        }

        public Armor GetArmor(int id)
        {
            throw new NotImplementedException();
        }
    }
}
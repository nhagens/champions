﻿using Champions.DAL.Interfaces;
using Champions.Models.Items;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Champions.DAL.Implementations
{
    public class WeaponContext : IItemContext, IWeaponContext
    {
        private readonly string connectionstring = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;

        public List<string> GetAllNames()
        {
            List<string> weaponNames = new List<string>();

            using (SqlConnection con = new SqlConnection(connectionstring))
            {
                con.Open();

                SqlCommand cmd = new SqlCommand("GetAllWeaponNames", con)
                {
                    CommandType = CommandType.StoredProcedure
                };

                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter
                {
                    SelectCommand = cmd
                };
                da.Fill(dt);

                foreach (DataRow row in dt.Rows)
                {
                    weaponNames.Add((string)row["Name"]);
                }

                con.Close();
            }

            return weaponNames;
        }

        public Weapon GetWeapon(int id)
        {
            throw new NotImplementedException();
        }

        public List<string> GetWeaponNamesOfType(string weaponType)
        {
            List<string> weaponNames = new List<string>();

            using (SqlConnection con = new SqlConnection(connectionstring))
            {
                con.Open();

                SqlCommand cmd = new SqlCommand("GetWeaponNames", con)
                {
                    CommandType = CommandType.StoredProcedure,
                };
                cmd.Parameters.Add("@weaponType", SqlDbType.VarChar);
                cmd.Parameters["@weaponType"].Value = weaponType;

                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter
                {
                    SelectCommand = cmd
                };
                da.Fill(dt);

                foreach (DataRow row in dt.Rows)
                {
                    weaponNames.Add((string)row["Name"]);
                }

                con.Close();
            }

            return weaponNames;
        }
    }
}
﻿using Champions.Models.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Champions.DAL.Interfaces
{
    public interface IWeaponContext : IItemContext
    {
        Weapon GetWeapon(int id);

        List<string> GetWeaponNamesOfType(string weaponType);
    }
}

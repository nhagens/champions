﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Champions.DAL.Interfaces
{
    public interface IItemContext
    {
        List<string> GetAllNames();
    }
}

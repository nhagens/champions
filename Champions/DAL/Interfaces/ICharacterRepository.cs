﻿using Champions.Models.Characters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Champions.DAL.Interfaces
{
    public interface ICharacterRepository
    {
        List<DataCharacter> GetAllPreview();
        DataCharacter GetCharacter(int id);
        void Add(DataCharacter character);
        void Update(DataCharacter character);
        void Remove(DataCharacter character);

    }
}

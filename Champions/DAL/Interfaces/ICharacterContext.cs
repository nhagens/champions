﻿using Champions.Models.Characters;
using System.Collections.Generic;

namespace Champions.DAL.Interfaces
{
    public interface ICharacterContext
    {
        void Add(DataCharacter character);

        void Remove(DataCharacter character);

        void Update(DataCharacter character);

        List<DataCharacter> GetAllPreview();

        DataCharacter GetCharacter(int id);
    }
}
